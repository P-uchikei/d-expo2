﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Point_script : MonoBehaviour
{
    public Text p1, p2;

    private int pp1 = 0, pp2 = 0;

    private GameManager Manager;


    void Start()
    {
        Manager = FindObjectOfType<GameManager>();
    }

	void Update ()
    {
        pp1 = Manager.CountR();
        pp2 = Manager.CountB();

        p1.text = "" + pp1;
        p2.text = "" + pp2;
	}
}