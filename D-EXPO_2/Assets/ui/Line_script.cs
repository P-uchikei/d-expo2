﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line_script : MonoBehaviour
{
    public GameObject st, mi;
    LineRenderer lr;

	void Start ()
    {
        lr = GetComponent<LineRenderer>();
	}
	
	void Update ()
    {
        lr.SetPosition(0, st.transform.position);
        lr.SetPosition(1, mi.transform.position);
	}
}