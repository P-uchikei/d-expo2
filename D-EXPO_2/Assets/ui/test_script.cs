﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test_script : MonoBehaviour
{
    public static int r = 0, count = 0;
    private GameManager GM;

    void Start()
    {
        GM = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        Vector3 mip = transform.position;

        r = GM.CountR();
        count = GM.CountB() + r;

        if (r == 0 && count == 0)
        {
            //初期位置
            mip.x = 0;
            transform.position = mip;
        }
        else
        {
            // count : r = 36 : mip.x
            mip.x = (float)36 * r / count - 18;
            transform.position = mip;
        }
    }
}