﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FullScreenSwitch : MonoBehaviour {

	public void Switch()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }
}
