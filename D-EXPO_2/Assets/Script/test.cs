﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour {

    public Vector3 position;
    private ConnectChecker C;

	// Use this for initialization
	void Start () {
        C = GetComponent<ConnectChecker>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            StartCoroutine("T");
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
           C.Debug_print();
        }
    }

    IEnumerator T()
    {
        yield return null;
        C.CheckCirculation(-1, gameObject, -1);
    }
}
