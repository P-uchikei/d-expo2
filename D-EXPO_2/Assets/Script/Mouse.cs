﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour {
    public GameObject trans;
   
    private float Z;
    public int mask;
    public float cansee = 100;
    public GameObject subparent;
    static public GameObject Hobj;
    private RectTransform rect;
    public float speed = 10f;
    private Vector3 screebtoworld;
    private Vector3 viewLocation;
    public float R = 90;
    static public GameObject panels;
    GameManager maneger;
    static public GameObject inspecter;
    void Start() {
        maneger = FindObjectOfType<GameManager>();
            }
    void Ray()
    {
        
        //カメラからマウスポインタの位置をへrayをとばす
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //当たってobjの指定・rayの距離の指定
        if (Physics.Raycast(mouseRay, out hit,Mathf.Infinity,mask))
        {
            //取得対象の親子が二重になっているため最も大きい親を取得する
            subparent = hit.collider.transform.parent.gameObject;
            Hobj = subparent.transform.parent.gameObject;
            inspecter = Hobj;
            //RayOn();
            //最も大きい親の現在のz軸上の角度取得
            Z = Hobj.transform.localRotation.z;
        }

        
        //デバック用、sceneビューでrayの視認
        Debug.DrawRay(mouseRay.origin, mouseRay.direction, Color.red, cansee);

    }

    // Update is called once per frame
    void Update() {
        if (maneger.ColorCheck() == 'B')
        {
            mask = LayerMask.GetMask("panel.blue");
        }
        else if (maneger.ColorCheck() == 'R')
        {
            mask = LayerMask.GetMask("panel.red");
        }
        //左クリックでrayで取得したobjの確認
        if (Input.GetMouseButtonDown(0))
        {
            //nullの場合rayで物体を取得しようとする
            if (Hobj == null)
            {
                Ray();
            }
            else
            {
                //すでに取得している場合取得したobjを放す
                while (Hobj != null)
                {
                    //RayOff();
                    Hobj = null;
                    subparent = null;
                }
            }
        }
        if (Input.GetKey(KeyCode.Space))
        {
            //Debug.Log(Hobj);
        }

        if (Hobj != null)
        {
            //rayでobjを取得している際マウスのポジションをobjが追跡するようにする
            viewLocation = Input.mousePosition;
            viewLocation.z = 10f;
            screebtoworld = Camera.main.ScreenToWorldPoint(viewLocation);
            screebtoworld.z = -2;
            Hobj.transform.position = screebtoworld;

            if (Input.GetMouseButtonDown(1))
            {
               
               //取得しているobjをz軸90度回転
                Z += R;
                foreach(Transform child in Hobj.transform)
                {
                    foreach (Transform childchild in child)
                    {

                        childchild.GetComponent<ConnectChecker>().Rotate(R);

                    }
                    
                }
                Hobj.transform.localRotation = Quaternion.Euler(0, 0, Z);
            }
        }
       

    }

    static public bool IsPutable()
    {
        




        
        //panel一つずつのray判定を区別する
        Transform tempPanels = inspecter.GetComponentInChildren<Transform>();
        
        foreach(Transform panel in tempPanels)
        {
            //Debug.Log(panel.position);

            
            Transform childBoard = panel.GetComponentInChildren<Transform>();
      
            foreach(Transform child in childBoard)
            {
                Panel_select panelSelect = child.gameObject.GetComponent<Panel_select>();
                if (panelSelect.GetIsPutalbe()) { return false; }
            }
            
            
        }
        return true;
    }
    //void RayOn()
    //{
    //    Transform piece = Hobj.GetComponentInChildren<Transform>();
    //    foreach(Transform childpanel in piece)
    //    {
    //        Transform panelray = childpanel.GetComponentInChildren<Transform>();
    //        foreach (Transform children in panelray)
    //        {
    //            children.gameObject.GetComponent<Panel_select>().enabled = true;
    //        }
    //    }
    //}
    //void RayOff()
    //{
    //    Transform piece2 = Hobj.GetComponentInChildren<Transform>();
    //    foreach (Transform childpanel2 in piece2)
    //    {
    //        Transform panelray2 = childpanel2.GetComponentInChildren<Transform>();
    //        foreach (Transform children2 in panelray2)
    //        {
    //            children2.gameObject.GetComponent<Panel_select>().enabled = false;
    //        }
    //    }
    //}
}
