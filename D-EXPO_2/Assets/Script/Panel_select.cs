﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel_select : MonoBehaviour
{
    public LayerMask Mask;
    public LayerMask LockMask;
    private Renderer color;
    public float cansee = 100;
    public GameObject substore;
    public GameObject inspecter;
    public GameObject storeobj;
    private Vector3 StartPosi;
    public static int swi = 1;
    public static int key = 1;
    private GameManager Manager;
    // Use this for initialization

    private bool isPutable = false;
    void Start()
    {
        substore = this.gameObject.transform.parent.gameObject;
        inspecter = substore.transform.parent.gameObject;
        //現在の位置を記憶(親子関係が二重になっているため元の親を取得)
        StartPosi = substore.transform.parent.position;
        //this.gameObject.GetComponent<Panel_select>().enabled = false;
        Manager = FindObjectOfType<GameManager>();

    }
    void Ray()
    {
        //panelからz軸平行にrayをとばす
        Ray panelRay = new Ray(this.transform.position, transform.forward * -1);
        RaycastHit hit;
        if (Physics.Raycast(panelRay, out hit, 50.0f, LockMask))
        {
            SetPutable(true);

        }else
        {
            if (storeobj != null)
            {
                SetPutable(false);
            }

        }
        
        if (Physics.Raycast(panelRay, out hit, 50.0f, Mask))
        {


            //nullチェック
            if (storeobj == null)
            {
                //現在のobjの情報を保持
                storeobj = hit.collider.gameObject;
            }

            if (storeobj != hit.collider.gameObject)
            {
                //現在のobjとrayが取得しているobjが違う場合現在取得しているobjを白にする
                storeobj.GetComponent<MeshRenderer>().material.color = Color.white;
                //現在のobjをrayが取得しているobjに更新
                storeobj = hit.collider.gameObject;
            }
            //rayで取得しているobjを赤色に
            hit.collider.GetComponent<MeshRenderer>().material.color = Color.red;
            
            if (Input.GetMouseButtonDown(0) && Mouse.IsPutable())
            {
                //panel生成するプログラムの制御key(swiが1であるときpanelが生成される)
                swi = 1;
                //rayで取得しているobjの位置へマウスでつかんでいるpanelを移動させる
                Vector3 select = hit.collider.gameObject.transform.position;
                select.z -= 0.5f;
                substore.transform.position = select;
                //パネルをまとめていた親を解除
                substore.transform.parent = null;
                //panelのlayermaskを変えることでマウスのrayの選択から外れる
                this.gameObject.layer = 11;
                //設置時の処理を行う。
                GetComponent<ConnectChecker>().Put();
                //Manager.TurnChange();
                //rayがありつつづけることで処理に負担がかかるためこのプログラム事破棄
                Destroy(this);
            }
            //hit.collider.GetComponent<MeshRenderer>().material.color = StoreColor; 
        }
        else
        {
            SetPutable(true);
            if (storeobj != null)
            {
                storeobj.GetComponent<MeshRenderer>().material.color = Color.white;
                storeobj = null;
            }
        }


        Debug.DrawRay(panelRay.origin, panelRay.direction, Color.red, cansee);
    }
    // Update is called once per frame
    void Update()
    {
        if (Mouse.Hobj != null) 
        if (Mouse.Hobj.transform.parent.gameObject == inspecter.transform.parent.gameObject)
        {
            Ray();
        }
        //rayの選択対象がない場合つかんでいるpanelを元の位置に戻す
        if (swi == 0 && Input.GetMouseButtonDown(0) && StartPosi != null)
        {
            substore.transform.parent.position = StartPosi;
        }
    }

    private void SetPutable(bool isPutable)
    {
        //panelが置けないときtrueをいれてください
        this.isPutable = isPutable;
    }
    public bool GetIsPutalbe()
    {
        return this.isPutable;
    }
}
