﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regenerate : MonoBehaviour {

    public int StartCount;
    public int Count = 0;

    GameObject[] panels;
    void Start()
    {
        Count = 0;
    }



	public void Regeneration()
    {
        List<GameObject> children = new List<GameObject>();
        Count++;
        
        if(Count >= StartCount)
        {
            Count = 0;
            int i = 0;
            foreach(Transform child in transform)
            {
                children.Add(child.gameObject.GetComponent<PanelGaneration>().Getchild());
            }
            panels = children.ToArray();

            if((panels[0].GetComponent<ConnectChecker>().pipe[3] == true && panels[1].GetComponent<ConnectChecker>().pipe[1] == false) ||
                panels[0].GetComponent<ConnectChecker>().pipe[3] == false && panels[1].GetComponent<ConnectChecker>().pipe[1] == true)
            {
                RE();
            }

            if(panels.Length == 3)
            {
                if ((panels[1].GetComponent<ConnectChecker>().pipe[0] == true && panels[2].GetComponent<ConnectChecker>().pipe[2] == false) ||
                   panels[1].GetComponent<ConnectChecker>().pipe[0] == false && panels[2].GetComponent<ConnectChecker>().pipe[2] == true)
                {
                    RE();
                }
            }

        }
      
       



    }
    private void RE()
    {
        foreach(GameObject panel in panels)
        {
            Destroy(panel);
        }
        foreach (Transform child in transform)
        {
            child.GetComponent<PanelGaneration>().ReStart();
        }
    }
}
