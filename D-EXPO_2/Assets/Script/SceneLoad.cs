﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoad : MonoBehaviour {


    public string sceneName = "GameMain";

    public void ScenceLoad()
    {
        gameObject.GetComponent<AudioSource>().Play();
        StartCoroutine("Load");
    }

    IEnumerator Load()
    {
        yield return new WaitForSeconds(0.1f);
        Application.LoadLevel(sceneName);
    }
}
