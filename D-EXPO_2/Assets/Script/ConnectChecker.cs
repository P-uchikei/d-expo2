﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//パネル接続確認関連

/// 最重要事項
/// 0:上
/// 1:右
/// 2:下
/// 3:左
/// 方向判別で上記の識別方法を使用
/// x座標正の方向を右
/// y座標正の方向を上とする。


public class ConnectChecker : MonoBehaviour {
    public static int UPPER = 0;
    public static int RIGHT = 1;
    public static int UNDER = 2;
    public static int LEFT = 3;


    
    public enum TeamColor
    {
        Nutral, Red, Blue
    }

    //パネル色確認用
    public TeamColor teamColor = TeamColor.Nutral;


    //接続済みパネル記憶
    private GameObject[] Connection = new GameObject[4];


    //循環確認時に使用
    public bool Discovered = false;

    //パイプ配置確認用
    public bool[] pipe = new bool[4];

    //管理者
    private GameManager Manager;


	// Use this for initialization
	void Start () {
        //Debug.Log(Connection[0]);
        Discovered = false;
        Manager = FindObjectOfType<GameManager>();
        ColorChange('G');
    }
	
	// Update is called once per frame
	void Update () {

	}

    //接続されたパネルを確認
    public void Debug_print()
    {
        for(int i = 0; i < 4; i++)
        {
            Debug.Log(Connection[i]);
        }
    }

    //Connectionに接続されたパネルを格納
    public void Connect(GameObject Obj)
    {
        int way = WayIdentifucation(Obj.transform.position);
        if(way >= 0 && way <= 3)
        {
            Connection[way] = Obj;
        }
        //CheckCirculation(-1);

    }

    public void Rotate(float R)
    {
        bool temp;
        
        if(R > 0)
        {
            temp = pipe[0];
            for(int i = 0; i < 3; i++)
            {
                pipe[i] = pipe[i + 1];
            }
            pipe[3] = temp;
        }
        if(R < 0)
        {
            temp = pipe[3];
            for(int i = 3; i > 0; i--)
            {
                pipe[i] = pipe[i - 1];
            }
            pipe[0] = temp;
        }
    }


    //つながったパネルの方向検知
    private int WayIdentifucation(Vector3 tf)
    {
        Vector3 myself = transform.position;
        //上
        if (myself.x == tf.x && myself.y < tf.y)
        {
            return UPPER;
        }
        //下
        if (myself.x == tf.x && myself.y > tf.y)
        {
            return UNDER;
        }
        //右
        if (myself.y == tf.y && myself.x < tf.x)
        {
            return RIGHT;
        }
        //左
        if (myself.y == tf.y && myself.x > tf.x)
        {
            return LEFT;
        }

        return -1;
    }

    //循環接続確認と色変え
    public void CheckCirculation(int way, GameObject Start, int StartingWay)
    {
        //Debug.Log("探索");
        Discovered = true;
        for(int i = 0; i < 4; i++)
        {

            ConnectChecker CC;

            if (way == i)
            {
                continue;
            }

           if(Connection[i] == null)
            {
                continue;
            }else
            {
                CC = Connection[i].GetComponent<ConnectChecker>();
            }

            if (Connection[i] == Start)
            {
                if (i != StartingWay)
                {
                    GameObject[] Panels = GameObject.FindGameObjectsWithTag("Panel");
                    foreach (GameObject p in Panels)
                    {
                        ConnectChecker pCC = p.GetComponent<ConnectChecker>();
                        if (pCC.Discovered)
                        {
                            pCC.ColorChange(Manager.ColorCheck());
                        }
                    }
                }
                continue;
            }

            if (CC.Discovered == false)
            {
                int W;
                if (StartingWay == -1)
                {
                    W = (i + 2) % 4;
                }
                else
                {
                    W = StartingWay;
                }

                CC.CheckCirculation((i + 2) % 4, Start, W);

            }
            else
            {
                continue;
            }
        }
        Discovered = false;
        return;
    }

    
    
    public void ColorChange(char C)
    {
        Color color;

        switch (C)
        {
            case 'R':
                color = new Color(1f, 0.25f, 0.125f, 1f);
                teamColor = TeamColor.Red;
                break;
            case 'B':
                color = new Color(0.25f, 0.4f, 1f, 1f);
                teamColor = TeamColor.Blue;
                break;
            default:
                color = new Color(0.2f, 1f, 0.4f, 1f);
                teamColor = TeamColor.Nutral;
                break;
        }
        Manager.count(C);
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<MeshRenderer>().material.color = color;
        }
    }


    public void Put()
    {
        int x = ((int)(Mathf.Round(transform.parent.position.x)) + 6) / 2;
        int y = ((int)(Mathf.Round(transform.parent.position.y)) + 6) / 2;
        Manager.panels[x, y] = gameObject;
        for (int i = 0; i < 4; i++)
        {
            GameObject next = null;
            if (i == UPPER && y < 6)
            {
                next = Manager.panels[x, y + 1];
            }else if(i == RIGHT && x < 6)
            {
                next = Manager.panels[x + 1, y];
            }else if(i == UNDER && y > 0)
            {
                next = Manager.panels[x, y - 1];
            }else if(i == LEFT && x > 0)
            {
                next = Manager.panels[x - 1, y];
            }
            if(next == null)
            {
                continue;
            }else
            {
                ConnectChecker CC = next.GetComponent<ConnectChecker>();
                if (pipe[i] == true && CC.pipe[(i + 2) % 4] == true)
                {
                    Connection[i] = next;
                    CC.Connection[(i + 2) % 4] = this.gameObject;
                }
            }
        }
        CheckCirculation(-1, gameObject, -1);
        Manager.PutPanel();
    }
}
