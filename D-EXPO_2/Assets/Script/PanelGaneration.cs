﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelGaneration : MonoBehaviour {
    public List<GameObject> Resouce = new List<GameObject>();
    private int num;
    private Quaternion rota;
    private float Z;
    private GameObject child;
    public GameObject parentsub;
    public GameObject parent;
    private int Rand;
	// Use this for initialization
	void Start () {
        //設定されたobjをランダムで生成する
        num = Random.Range(0, Resouce.Count);
        //生成されたobjのz軸の角度を90度単位で変化させて生成する
        Rand = Random.Range(0, 4);
        Z = Rand * 90f;
        
        rota = Quaternion.Euler (0, 180, Z);
        child = Instantiate(Resouce[num], this.transform.position,rota);
        child.transform.parent = this.gameObject.transform;
        parentsub = this.gameObject.transform.parent.gameObject;
        parent = parentsub.transform.parent.gameObject;
        if(parent.transform.parent.gameObject.name == "blue")
        {
            child.layer = 10;
        }else if(parent.transform.parent.gameObject.name == "red")
        {
            child.layer = 9;
        }
        for(int i = 0; i < Rand; i++)
        {
            //Debug.Log("kaiten" + gameObject);
            child.GetComponent<ConnectChecker>().Rotate(-90f);
        }
        transform.parent.gameObject.GetComponent<Regenerate>().Regeneration();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public GameObject Getchild()
    {
        return child;
    }

    public void ReStart()
    {
        Start();
    }
}
