﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Awaking : MonoBehaviour {

    public int ScreenWidth;
    public int ScreenHeight;
    void Awake()
    {
        // PC向けビルドだったらサイズ変更
        if (Application.platform == RuntimePlatform.WindowsPlayer ||
        Application.platform == RuntimePlatform.OSXPlayer ||
        Application.platform == RuntimePlatform.LinuxPlayer)
        {

            Screen.SetResolution(ScreenWidth, ScreenHeight, Screen.fullScreen);
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
