﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generation : MonoBehaviour {
    public List<GameObject> Resouce = new List<GameObject>();
    private int sw1 = 1;
    private int num;
    private GameObject temobj;
    private bool init = false;
    private GameManager Manager;
    // Use this for initialization
    void Start () {

        Manager = FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
        //panel生成する仮置きobjの生成
        num = Random.Range(0, Resouce.Count);
        if (Resouce[num] != null && sw1==1 && this.gameObject.transform.childCount == 0)
        {
            Manager.TurnChange();
            if (init)
            {
                gameObject.GetComponent<AudioSource>().Play();
            }else
            {
                init = true;
            }
            temobj = (GameObject)Instantiate(Resouce[num],this.transform.position,Quaternion.identity );
            //Mouse.panels = (GameObject)temobj;
            temobj.transform.parent = this.gameObject.transform;
            sw1 = 0;
            Panel_select.swi = 0;
        }
	}
    //仮置きが生成場所にある場合生成されない鍵
    void OnTriggerStay(Collider other)
    {
        sw1 = 0;
    }

    void OnTriggerExit(Collider other)
    {
        sw1 = 1;
    }
}
