﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    //赤パネル数
    private int CountRed = 0;
    //青パネル数
    private int CountBlue = 0;
    //現在のターン
    private ConnectChecker.TeamColor Turn = ConnectChecker.TeamColor.Nutral;

    //設置されたパネルを記録
    public GameObject[, ] panels = new GameObject[7, 7];

    private int PanelCount = 0;

    public GameObject red;
    public GameObject blue;

    public GameObject WBlue;
    public GameObject WRed;
    public GameObject WDraw;

	// Use this for initialization
	void Start () {
        if(Random.Range(0, 2) == 0)
        {
            red.SetActive(true);
            blue.SetActive(false);
            Turn = ConnectChecker.TeamColor.Red;
        }else
        {
            red.SetActive(false);
            blue.SetActive(true);
            Turn = ConnectChecker.TeamColor.Blue;
        }
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    public void TurnChange()
    {
        ReCount();
        if(Turn == ConnectChecker.TeamColor.Red)
        {
            red.SetActive(false);
            blue.SetActive(true);
            Turn = ConnectChecker.TeamColor.Blue;
        }else
        {
            red.SetActive(true);
            blue.SetActive(false);
            Turn = ConnectChecker.TeamColor.Red;
        }
    }

    public void ForcedTurnChange(char TEAM)
    {
        switch (TEAM)
        {
            case 'R':
                Turn = ConnectChecker.TeamColor.Red;
                break;
            case 'B':
                Turn = ConnectChecker.TeamColor.Blue;
                break;
            default:
                break;
        }
    }


    public void PutPanel()
    {
        PanelCount++;
        if (PanelCount >= 49) GameOver();
    }


    private void GameOver()
    {
        gameObject.GetComponent<AudioSource>().Stop();

        if(CountRed > CountBlue)
        {
            WRed.SetActive(true);
        }else if(CountBlue > CountRed)
        {
            WBlue.SetActive(true);
        }else
        {
            WDraw.SetActive(true);
        }

        Destroy(FindObjectOfType<Mouse>());
        Destroy(red);
        Destroy(blue);


    }


    //パネル数カウント
    public void count(char TEAM)
    {
        switch (TEAM)
        {
            case 'R':
                CountRed++;
                break;
            case 'B':
                CountBlue++;
                break;
            default:
                break;
        }
    }

    public char ColorCheck()
    {
        if (Turn == ConnectChecker.TeamColor.Red)
        {
            return 'R';
        }
        else
        {
            return 'B';
        }
    }


    //パネル数再カウント
    private void ReCount()
    {
        CountBlue = 0;
        CountRed = 0;
        GameObject[] Panels = GameObject.FindGameObjectsWithTag("Panel");
        foreach (GameObject p in Panels)
        {
            ConnectChecker pCC = p.GetComponent<ConnectChecker>();
            switch (pCC.teamColor)
            {
                case ConnectChecker.TeamColor.Blue:
                    CountBlue++;
                    break;
                case ConnectChecker.TeamColor.Red:
                    CountRed++;
                    break;
                default:
                    break;
                
            }
        }
    }

    public int CountR()
    {
        return CountRed;
    }
    public int CountB()
    {
        return CountBlue;
    }
}